var mesList = Array(...document.querySelector("#message").children);
var memberDOMList = Array.from(document.querySelectorAll("tbody tr td"));
//  Info = ["name", "state(job)", "name2"...]
var memberInfoList = memberDOMList.map((item, i) => {
    return memberDOMList[i].innerText;
});
// 観戦者を抜いた参戦者の名前<List>、鯖もついでに入れる。
var players = memberInfoList.filter((item, i) => {
    return memberInfoList[i + 1] !== "観戦者 (観戦者)" && i % 2 === 0;
}).concat(["鯖"]);
var mes = [];
var player = "";
var mesTo = "";
var colors = [];
for (var i = 0; i < players.length; i++){
    colors.push(`hsl(${360 / players.length * i}, 100%, ${50 + 20 * (i % 2)}%)`);
}
for (var i = 1; i < mesList.length; i++) {
    mes = mesList[i].children;
    player = mes[0].innerHTML;
    if (players.indexOf(player) === -1){
        mesTo = player.split("→").slice(-1)[0];
        if (mesTo !== player){
            if (mesTo === "人狼") {
                mes[0].innerHTML = "人狼";
                mes[1].innerHTML = ": アオーーーーン"
            } else {
                mes[0].parentNode.innerHTML = ""
            }
        }
    } else {
        if (player === "鯖"){
            mes[1].style.color = "skyblue"
        } else {
            mes[1].style.color = colors[players.indexOf(player)]
        }
    }
}
for (var i = 1; i < memberDOMList.length; i = i + 2){
    status = memberDOMList[i].innerHTML.split(" ").join("").split("\n").join("");
    if (status !== "観戦者(観戦者)"){
        memberDOMList[i].innerHTML = status.split("(")[0] + "(参戦者)"
    }
}
