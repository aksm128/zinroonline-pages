var mesList = Array(...document.querySelector("#message").children);
var memberDOMList = Array.from(document.querySelectorAll("tbody tr td"));
//  Info = ["name", "state(job)", "name2"...]
var memberInfoList = memberDOMList.map(function(item, i){return memberDOMList[i].innerText;});
// 観戦者を抜いた参戦者の名前<List>、鯖もついでに入れる。
var players = memberInfoList.filter(function(item, i){return memberInfoList[i + 1] !== "観戦者 (観戦者)" && i % 2 === 0;}).concat(["鯖"]);
var mes = [];
var userForm = window.prompt("消したい名前をカンマ区切りで入力してください");
var blockList = userForm.split(",");
var player = "";
for (var i = 1; i < mesList.length; i++) {
    mes = mesList[i].children;
    player = mes[0].innerHTML;
    if (player.indexOf("→") !== -1){
        // 夜時間考慮
        player = player.split("→").slice(0,-1).join("→");
    }
    if (mes[1] === undefined){
        continue;
    }
    if ((mes[1].innerHTML.length > 100 || mes[1].innerHTML.split("\n").length - 1 >= 7) && players.indexOf(player) === -1) {
        mes[1].innerHTML = "**blocked**"
    }
    if (blockList.indexOf(player) !== -1){
        mesList[i].remove();
    }
}
