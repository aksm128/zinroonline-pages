/* eslint-disable no-console */
console.log("point 1 OK.");

/// global
let _finalLogDate = getFinalLogDate();
let _data = {};
let _winPer = {};
let _winChart;
///

/////////////////////////////////////////////////////////////////////// chart.js
console.log("point 2 OK.");

function getParam(){
  let arg = new Object;
  const pair = location.search.substring(1).split('&');
  for(var i = 0; pair[i]; i++) {
    let kv = pair[i].split('=');
    arg[kv[0]] = kv[1];
  }
  return arg;
}
console.log("point 3 OK.");


function getAnalyzedData(names, func){
  const url = "https://zinrologanalyzerapi.herokuapp.com/";
  // const url = "http://localhost:5000/";
  let numOfLog = 0;
  $.get(url+"numOfLog", function(data){
    numOfLog = JSON.parse(data)["numOfLog"];
    let tmpData = {};
    let now = parseInt(1/(numOfLog+1)*100);
    $("#loading").html("<center>loading</center><div class=\"progress\"><div class=\"progress-bar progress-bar-striped progress-bar-animated\" role=\"progressbar\" aria-valuenow=\""+String(now)+"\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: "+String(now)+"%\"></div></div>");

    (async function() {
      for(var i = 0; i < numOfLog + 1; i++){
        await $.ajax({
          type: "post",
          url: url+"analyzer/getData",
          data: JSON.stringify({
            "names": names,
            "num": i,
            "tmpData": tmpData
          }),
          contentType: 'application/json',
          success: function(data){
            if (i == numOfLog){
              $.ajax({
                type: "post",
                url: url+"analyzer/dumpData",
                data: JSON.stringify({
                  "names": names,
                  "data": JSON.parse(data),
                }),
                contentType: 'application/json',
                success:function(data){
                  tmpData = JSON.parse(data);
                  $("#loading").empty();
                  func(tmpData);
                },
                error:function(xhr, errorText,){
                  $("#alert").append("<div class=\"alert alert-danger m-2\">"+errorText+": 通信に失敗しました</div>");
                  $("#loading").empty();
                }
              });
            }else{
              tmpData = JSON.parse(data);
              now = parseInt((i+2)/(numOfLog+1)*100);
              $("#loading").html("<center>loading</center><div class=\"progress\"><div class=\"progress-bar progress-bar-striped progress-bar-animated\" role=\"progressbar\" aria-valuenow=\""+String(now)+"\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: "+String(now)+"%\"></div></div>");
            }
          },
          error:function(xhr, errorText){
            $("#alert").append("<div class=\"alert alert-danger m-2\">"+errorText+": 通信に失敗しました</div>");
            $("#loading").empty();
          }
        });
        console.log(i, tmpData);
      }
    })();
  }).fail(function(xhr, errorText){
    $("#alert").append("<div class=\"alert alert-danger m-2\">"+errorText+": 通信に失敗しました</div>");
    $("#loading").empty();
  });
}
console.log("point 4 OK.");

function jobColorsFill(jobList) {
  const jobs = ["占","霊","狩","猫","共","村","狼","狂","信","狐","背"];
  const jobColors = [
    "rgba(128,255,255,0.7)",
    "rgba(0,128,255,0.7)",
    "rgba(175,95,19,0.7)",
    "rgba(153,0,112,0.7)",
    "rgba(15,140,12,0.7)",
    "rgba(10,73,86,0.7)",
    "rgba(255,0,0,0.7)",
    "rgba(255,0,255,0.7)",
    "rgba(255,0,128,0.7)",
    "rgba(255,255,0,0.7)",
    "rgba(128,128,0,0.7)",
  ];
  let outputColors = [];
  for (let i = 0; i < jobList.length; i++) {
    for (let j = 0; j < jobs.length; j++) {
      if (jobList[i] == jobs[j]) {
        outputColors.push(jobColors[j]);
      }
    }
  }
  return outputColors;
}

console.log("point 5 OK.");

function winPerChartUpdate(winChart, jobs, winPer) {
  winChart.data.labels = jobs;
  winChart.data.datasets[0].backgroundColor = jobColorsFill(jobs);
  winChart.data.datasets[0].data = winPer;
  console.log("OK");
  winChart.update();
}
console.log("point 6 OK.");

// function sumDict(dict){
//   let sum = 0;
//   for (let key in dict){
//     sum += dict[key];
//   }
//   return sum;
// }
console.log("point 7 OK.");

$(function(){
  let winCtx = document.getElementById("winChart").getContext("2d");
  _winChart = new Chart(winCtx, {
    "type": 'polarArea',
    "data": {
      "labels": [],
      "datasets": [
        {
          "backgroundColor": [],
          "data": []
        }
      ]
    },
    "options": {
      "respondable": true,
      "legend": {
        "fontSize": 10
      },
      "scale": {
        "ticks": {
          "min": 0,
          "max": 80,
          "fontSize": 12
        }
      }
    }
  });

  let timeCtx = document.getElementById("enteringRoomTimeChart").getContext("2d");
  let _enterRoomTimeChart = new Chart(timeCtx, {
    "type": 'bar',
    "data": {
      "datasets":[{
        "title": "時間別の参戦回数",
        "label": "参戦回数",
        "borderColor": "rgba(0,0,0,0)",
        "backgroundColor": "rgba(100,100,255,0.9)",
        "pointRadius": 50,

        "data": []
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "type": "time",
          "time": {
            "unit": "hour",
            "displayFormats": {
              "hour": "HH:mm:00"
            }
          }
        }]
      },
      "tooltips": {
        "callbacks": {
          "title": function(tooltipItem,){
            return (
              String(new Date(Number(new Date(tooltipItem[0].xLabel)))).slice(16,24)
              + "~" + String(new Date(Number(new Date(tooltipItem[0].xLabel))+1000*60*29)).slice(16,24)
            );
          },
          "label": function(tooltipItem,){
            return [tooltipItem.yLabel + "戦"];
          }
        }
      }
    }
  });

  let dateCtx = document.getElementById("enteringRoomDateChart").getContext("2d");
  let _enterRoomDateChart = new Chart(dateCtx, {
    "type":"bar",
    "data": {
      "datasets":[{
        "type": "line",
        "title": "月別の勝率",
        "label": "勝率",
        "pointBackgroundColor": "rgba(255,200,50,0.8)",
        "borderColor": "rgba(255,200,50,0.8)",
        "backgroundColor": "rgba(255,200,50,0.8)",
        "fill": false,
        "pointRadius": 5,
        "pointStyle": 'rect',
        "yAxisID": "y-axis-2", // 追加
        "data": []
      },{
        "title": "月別の参戦回数",
        "label": "参戦回数",
        "borderColor": "rgba(0,0,0,0)",
        "backgroundColor": "rgba(100,100,255,0.9)",
        "yAxisID": "y-axis-1", // 追加
        "data": []
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "type": "time",
          "time": {
            "unit": "month",
            "displayFormats": {
              "month": "YYYY-MM"
            }
          }
        }],
        "yAxes": [{
          "id": "y-axis-1",   // Y軸のID
          "type": "linear",   // linear固定
          "position": "left", // どちら側に表示される軸か？
          // ticks: {          // スケール
          //     max: 0.2,
          //     min: 0,
          //     stepSize: 0.1
          // },
        }, {
          "id": "y-axis-2",
          "type": "linear",
          "position": "right",
          "ticks": {
            "max": 100,
            "min": 0,
            "stepSize": 10
          },
        }],
      },
    }
  });
  // md以下ならグラフを全画面に
  if (window.innerWidth < 768){
    $(".chart-expand").attr("width", "100%");
    $(".chart-expand").attr("height", "100%");
  }

  // tooltip用の処理
  $('[data-toggle="tooltip"]').tooltip();
  // html2canvasで邪魔になる上隠しても問題ないので隠す
  $("input[type=radio]").hide();
  // 勝敗一覧
  $("#winLoseTable").hide();
  $("#winLoseTableToggle").click(function(){
    $("#winLoseTable").toggle();
  });

  console.log("point 8 OK.");

  const names = decodeURIComponent(getParam().names).split(",");
  getAnalyzedData(names,function(data){
    if (data["error"].filter(function(error){return error["no"] == 2;}).length == 1){
      $("#alert").append("<div class=\"alert alert-danger m-2\">ERROR: ログが存在しません。</div>");
    }
    _data = data;
    _winPer = {};
    for (let setting in _data["winLoseData"]){
      _winPer[setting] = [];
      for (let job in _data["winLoseData"][setting]["winPer"]["jobs"]){
        if (job == "(怪盗)"){ // 関係なデータまで来たら脱出（クソ雑魚データ構造くん）
          break;
        }
        if (_data["winLoseData"][setting]["winPer"]["jobs"][job] == -1){
          _winPer[setting].push(0);
          continue;
        }
        _winPer[setting].push(_data["winLoseData"][setting]["winPer"]["jobs"][job]);
      }
    }

    console.log("point 9 OK.");

    const jobs = ["占","霊","狩","猫","共","村","狼","狂","信","狐","背"];
    winPerChartUpdate(_winChart, jobs, _winPer["total"]);
    const name = names[0];
    $("#name").text(name);


    let totalGames = _data["winLoseData"]["total"]["games"]["teams"]["total"];
    let totalWin = _data["winLoseData"]["total"]["win"]["teams"]["total"];
    $("#totalGames").text(totalGames);
    $("#totalWin").text(totalWin);
    $("#totalLose").text(totalGames - totalWin);
    $("#totalWinPercent").text(Math.round(totalWin / totalGames * 10000)/100);

    console.log("point 10 OK.");

    for (let job in _data["winLoseData"]["total"]["games"]["jobs"]){
      $("#jobsTable").append(
        "<tr><th scope=\"raw\">" + job +
        "</th><th>" + _data["winLoseData"]["total"]["games"]["jobs"][job] +
        "</th><th>" + _data["winLoseData"]["total"]["win"]["jobs"][job] +
        "</th><th>" + [_data["winLoseData"]["total"]["winPer"]["jobs"][job], "-"][(_data["winLoseData"]["total"]["winPer"]["jobs"][job] == -1)*1] +
        "</th></tr>"
      );
    }
    for (let team in _data["winLoseData"]["total"]["games"]["teams"]){
      $("#jobsTable").append(
        "<tr><th scope=\"raw\">" + team +
        "</th><th>" + _data["winLoseData"]["total"]["games"]["teams"][team] +
        "</th><th>" + _data["winLoseData"]["total"]["win"]["teams"][team] +
        "</th><th>" + [_data["winLoseData"]["total"]["winPer"]["teams"][team], "-"][(_data["winLoseData"]["total"]["winPer"]["teams"][team] == -1)*1] +
        "</th></tr>"
      );
    }
    // const tweetText = name+"の勝率は"+String(Math.round(totalWin / totalGames * 10000)/100)+"%です！";
    // const chartURL = document.getElementById("winChart").toDataURL("image/png");
    // $("#share").append("<a id=\"share\" href=\"https://twitter.com/share\" class=\"twitter-share-button\"data-url=\"https://aksm128.github.io/zinroLogAnalyzer-Pages/\"data-text=\""+tweetText+"\"data-lang=\"ja\"data-hashtags=\"人狼online\">ツイート</a>\n<script src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>");
    // $("#tweetImageURL").attr("content", chartURL)

    let enterByTime = [];
    for (let i = 0; i < _data["enterByTime"].length; i++){
      enterByTime.push({
        "x": moment(1000*60*30*(i + 30)),
        "y": _data["enterByTime"][i]
      });
    }
    console.log(enterByTime);
    _enterRoomTimeChart.data.datasets[0].data = enterByTime;
    _enterRoomTimeChart.options.scales.yAxes["0"].ticks.min = 0;
    _enterRoomTimeChart.update();

    console.log("point 11 OK.");


    let enterByDate = [];
    for (let date in _data["enterByDate"]){
      //  例外処理
      if (moment(date, "YYYY-MM").isBefore(moment(_finalLogDate, "YYYY-MM-DD")) == false){
        continue;
      }
      enterByDate.push({
        "x": moment(date, "YYYY-MM"),
        "y": _data["enterByDate"][date]
      });
    }
    enterByDate.sort(function(a, b){
      return moment(a["x"], "YYYY-MM") - moment(b["x"], "YYYY-MM");
    });

    let winPerByDate = [];
    for (let date in _data["winPerByDate"]){
      //  例外処理
      if (moment(date, "YYYY-MM").isBefore(moment(_finalLogDate, "YYYY-MM-DD")) == false){
        continue;
      }
      winPerByDate.push({
        "x": moment(date, "YYYY-MM"),
        "y": _data["winPerByDate"][date]
      });
    }
    winPerByDate.sort(function(a, b){
      return moment(a["x"], "YYYY-MM") - moment(b["x"], "YYYY-MM");
    });

    _enterRoomDateChart.data.datasets[1].data = enterByDate;
    _enterRoomDateChart.data.datasets[0].data = winPerByDate;
    _enterRoomDateChart.options.scales.yAxes["0"].ticks.min = 0;
    _enterRoomDateChart.update();

    console.log("point 12 OK.");

    for (let i = 0;i < 5; i++){
      $("#" + String(i+1) + "thFriend").text(`${_data["ranking"]["friends"][i][0]} (${_data["ranking"]["friends"][i][1]})`);
      $("#" + String(i+1) + "thVillage").text(`${_data["ranking"]["villages"][i][0]} (${_data["ranking"]["villages"][i][1]})`);
    }
    $("#firstLog").append("<a href=\"https://zinro.net/m/log.php?id="+_data["firstLog"][0]+"\" target=\"_blank\">"+_data["firstLog"][1]+"</a>");
    $("#finalLog").append("<a href=\"https://zinro.net/m/log.php?id="+_data["finalLog"][0]+"\" target=\"_blank\">"+_data["finalLog"][1]+"</a>");
  });

  console.log("point 13 OK.");
  $("#total").click(function(){
    console.log("hoge");
  });

  $("#total,#12Acat,#11A,#12B,#8CFO,#10A,#14Dcat").click(function(){
    console.log("setting click OK");
    const settingJobs = {
      "total": ["占","霊","狩","猫","共","村","狼","狂","信","狐","背"],
      "12Acat": ["占","霊","狩","猫","村","狼","狂"],
      "11A": ["占","霊","狩","村","狼","狂"],
      "12B": ["占","霊","狩","村","狼","狂","狐"],
      "8CFO": ["占","霊","共","村","狼","狂"],
      "10A": ["占","霊","狩","村","狼","狂"],
      "14Dcat": ["占","霊","狩","猫","共","村","狼","信","狐","背"]
    };
    console.log("setting click 2 OK");
    const setting = $(this).attr("id");
    console.log(setting, _winChart, settingJobs[setting], _winPer[setting]);
    winPerChartUpdate(_winChart, settingJobs[setting], _winPer[setting]);
    console.log("setting click 3 OK");
    let totalGames = _data["winLoseData"][setting]["games"]["teams"]["total"];
    let totalWin = _data["winLoseData"][setting]["win"]["teams"]["total"];
    $("#totalGames").text(totalGames);
    $("#totalWin").text(totalWin);
    $("#totalLose").text(totalGames - totalWin);
    $("#totalWinPercent").text(Math.round(totalWin / totalGames * 10000)/100);
    $("#jobsTable").empty();
    console.log("setting click 4 OK");

    for (let job in _data["winLoseData"][setting]["games"]["jobs"]){
      $("#jobsTable").append(
        "<tr><th scope=\"raw\">" + job +
        "</th><th>" + _data["winLoseData"][setting]["games"]["jobs"][job] +
        "</th><th>" + _data["winLoseData"][setting]["win"]["jobs"][job] +
        "</th><th>" + [_data["winLoseData"][setting]["winPer"]["jobs"][job], "-"][(_data["winLoseData"][setting]["winPer"]["jobs"][job] == -1)*1] +
        "</th></tr>"
      );
    }
    console.log("setting click 5 OK");

    for (let team in _data["winLoseData"][setting]["games"]["teams"]){
      $("#jobsTable").append(
        "<tr><th scope=\"raw\">" + team +
        "</th><th>" + _data["winLoseData"][setting]["games"]["teams"][team] +
        "</th><th>" + _data["winLoseData"][setting]["win"]["teams"][team] +
        "</th><th>" + [_data["winLoseData"][setting]["winPer"]["teams"][team], "-"][(_data["winLoseData"][setting]["winPer"]["teams"][team] == -1)*1] +
        "</th></tr>"
      );
    }
  });
});
console.log("point 14 OK.");
