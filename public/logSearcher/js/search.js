/* eslint-disable no-console */
var _index = 0;
var _gameLists = [];

function getParamURIEncoded() {
  var arg = new Object;
  const pair = location.search.substring(1).split('&');
  for(var i = 0; pair[i]; i++) {
    var kv = pair[i].split('=');
    arg[kv[0]]=decodeURIComponent(kv[1]);
  }
  return arg;
}

const getSearchedData = (url) => new Promise((resolve, reject) => {
  let gameLists = [];
  const params = getParamURIEncoded();
  const playersNames = params["playersNames"].split("\\").map((arr) => {return arr.split(",");});
  const jobs = params["jobs"].split(",");
  const villageNames = params["villageNames"].split(",");
  const duringPairs = params["duringPairs"].split("\\").map((arr) => {return arr.split(",");});
  let settings = [];
  if (params["12Acat"] == "on"){
    settings.push("12Acat");
  }
  if (params["12B"] == "on"){
    settings.push("12B");
  }
  if (params["11A"] == "on"){
    settings.push("11A");
  }
  if (params["8CFO"] == "on"){
    settings.push("8CFO");
  }
  if (params["10A"] == "on"){
    settings.push("10A");
  }
  if (params["14Dcat"] == "on"){
    settings.push("14Dcat");
  }
  if (params["elseSetting"] == "on"){
    settings.push("undefined");
  }
  const escapeAtMark = (params["escapeAtMark"] == "on") * 1;
  const ignoreNoGame = (params["ignoreNoGame"] == "on") * 1;
  const ignoreWatcher = (params["ignoreWatcher"] == "on") * 1;

  console.log({
    "data": _gameLists,
    "settings": [
      playersNames,
      jobs,
      villageNames,
      settings,
      duringPairs,
      escapeAtMark,
      ignoreNoGame,
      ignoreWatcher
    ]
  });
  $.get(url+"numOfLog", (data) => {
    const numOfLog = JSON.parse(data)["numOfLog"];
    let now = 0;
    (async () => {
      for(let i = 0;i < numOfLog + 1; i++){
        await $.ajax({
          type: "post",
          url: url + "search/getData",
          data: JSON.stringify({
            "num": i,
            "data": gameLists,
            "setting": [
              playersNames,
              jobs,
              villageNames,
              settings,
              duringPairs,
              escapeAtMark,
              ignoreNoGame,
              ignoreWatcher
            ]
          }),
          contentType: "application/json",
          success: (data) => {
            gameLists = JSON.parse(data);
            now = parseInt((i+2)/(numOfLog+1)*100);
            $("#loading").html(`
            <center>loading</center>
            <div class="progress">
              <div
                class="progress-bar progress-bar-striped progress-bar-animated"
                role="progressbar"
                aria-valuenow="`+String(now)+`"
                aria-valuemin="0"
                aria-valuemax="100"
                style="width: `+String(now)+`%"
              ></div>
            </div>`);
          },
          error: (xhr, errorText) => {
            $("#alert").html("<div class=\"alert alert-danger m-2\">"+errorText+": 通信に失敗しました</div>");
            $("#loading").empty();
            reject(errorText);
          }
        });
      }
      await console.log(gameLists);
      resolve(gameLists);
      $("#loading").empty();
    })();
  }).fail((xhr, errorText) => {
    $("#alert").html("<div class=\"alert alert-danger m-2\">"+errorText+": 通信に失敗しました</div>");
    $("#loading").empty();
  });
});

const loopIndex = (min, now, max) => {
  return [...Array(max-min).keys()][(now % (max - min) + (max - min)) % (max - min)];
};

const updateListAndPagenation = (index, gameLists) => {
  $("#searchedData").empty();
  for (let game of gameLists.slice(_index * 50,_index * 50 + 50)){
    $("#searchedData").append(`
      <div class="border-bottom">
        <a href="https://zinro.net/m/log.php?id=${game["id"]}" target="_blank">${game["v"]}</a><br>
        ${game["s"]}<br>
        <span style="font-size:8px">
          ${Object.keys(game["m"]).map(function(mem){return mem + this[mem]["j"];}, game["m"])}
        </span><br>
        ${"<span class=\"text-danger\">無効試合の可能性有</span>".repeat(game["n"])}<br>
        <span style="font-size:8px">
          ${game["t"]}
        </span><br>
      </div>
    `);
  }
  $(".page-back-2").text(loopIndex(0, index - 2, Math.ceil(gameLists.length / 50)) + 1);
  $(".page-back-1").text(loopIndex(0, index - 1, Math.ceil(gameLists.length / 50)) + 1);
  $(".page-current").text(loopIndex(0, index, Math.ceil(gameLists.length / 50)) + 1);
  $(".page-next-1").text(loopIndex(0, index + 1, Math.ceil(gameLists.length / 50)) + 1);
  $(".page-next-2").text(loopIndex(0, index + 2, Math.ceil(gameLists.length / 50)) + 1);
};

$(() => {
  $("#result").hide();
  (async () => {
    const url = "https://zinrologanalyzerapi.herokuapp.com/";
    // const url = "http://localhost:5000/";
    _gameLists = await getSearchedData(url); // _gameLists insert parsed APIResponce
    $("#result").show();
    updateListAndPagenation(_index, _gameLists);
    // 読み込み終わってから有効に
    $(".page-back-2").click(() => {
      _index = loopIndex(0, _index - 2, Math.ceil(_gameLists.length / 50));
      updateListAndPagenation(_index, _gameLists);
    });
    $(".page-back-1").click(() => {
      _index = loopIndex(0, _index - 1, Math.ceil(_gameLists.length / 50));
      updateListAndPagenation(_index, _gameLists);
    });
    $(".page-next-1").click(() => {
      _index = loopIndex(0, _index + 1, Math.ceil(_gameLists.length / 50));
      updateListAndPagenation(_index, _gameLists);
    });
    $(".page-next-2").click(() => {
      _index = loopIndex(0, _index + 2, Math.ceil(_gameLists.length / 50));
      updateListAndPagenation(_index, _gameLists);
    });
    $("#formatWiki").click(() => {
      $("#formatedData").html(`<div class="border m-2"><div>`);
      for(let game of _gameLists){
        $("#formatedData div").append(`
          <a href="https://zinro.net/m/log.php?id=${game["id"]}" target="_blank">
            ${game["id"]}:
            (${game["v"]})
            ${game["t"]}
          </a><br>
        `);
      }
      $("#formatedData").append(`<button id="emptyFormatedData" class="btn btn-outline-primary m-2">閉じる</button>`);
      $("#emptyFormatedData").click(() => {
        $("#formatedData").empty();
      });
    });
  })();
});
